

# TECboot -> Bootloader for TEC
This is the code for the Bootloader.

## Installation
Please install STM32CubeIDE and upload the Code via programmer

## Openblt
OpenBLT is an open source bootloader for STM32, XMC, HCS12 and other microcontroller targets.

OpenBLT enables you and your customers to update the firmware on your microcontroller based product. A major benefit of OpenBLT being open source is that you can customize and tweak the bootloader to your specific needs.

OpenBLT can be ported to any 8-bit, 16-bit, or 32-bit microcontroller and currently runs on runs on ST STM32, Infineon XMC, NXP S12, and TI TM4C and LM3S microcontrollers. It is most popular on STM32 microcontrollers.

By default, it supports communication interfaces such as: RS232, CAN, USB, TCP/IP and it ships with the easy-to-use [MicroBoot](https://www.feaser.com/openblt/doku.php?id=manual:microboot) PC tool for initiating and monitoring the firmware update. Performing firmware updates directly from an SD-card is also supported.

For those that prefer a command line program for performing firmware updates, there is [BootCommander](https://www.feaser.com/openblt/doku.php?id=manual:bootcommander). If you would rather build your own firmware update tool for seamless integration into your existing toolset, then have a look at the OpenBLT host library ([LibOpenBLT](https://www.feaser.com/openblt/doku.php?id=manual:libopenblt)). This is an easy-to-use shared library, enabling you to develop your own firmware update tool quickly, in the programming language of your liking.

Note that all host tools (MicroBoot/BootCommander/LibOpenBLT) are cross-platform supporting both MS Windows and GNU/Linux.

The OpenBLT [download package](https://www.feaser.com/openblt/doku.php?id=download) contains numerous preconfigured demo programs for popular and low-cost development boards, making it possible for you to get started with the bootloader quickly.

## Upload Tecware
The Tecware is [here](https://gitlab.psi.ch/coldbox/tec/tecware).
There under Releases is the binary for uploading.

In this [Repo](https://gitlab.psi.ch/coldbox/rpi/rpi_uploadtec) you find the code to upload it via the CAN Bus on a Raspberry PI. If you prefer something else to upload, you can use [MicroBoot](https://www.feaser.com/openblt/doku.php?id=manual:microboot) for PC.